#!/bin/bash -e

echo "Execute change image docker compose."
original_string="$1"
replacement_string=$(echo "$original_string" | sed 's/\//\\\//g')
file="$2"
sed -i "s/__IMAGE__/$replacement_string/" $file
echo "Original string: $original_string"
echo "Replacement string: $replacement_string"
echo "Done change image."
